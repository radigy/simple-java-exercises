package Euler;

import java.util.Scanner;

public class Euler1 {

    public static void main(String[] args) {

        System.out.println("Suma podzielnosci 3 i 5 " + sumOfMultiplies(1000));

    }

    public static int sumOfMultiplies(int n) {

        int i;
        int j;
        int wynik = 0;

        for (i = 1; i < n; i++) {
            for (j = 1; j < i; j++) {
                if (i / 5.0 == j) {
                    wynik = wynik + i;
                    break;
                }
                if (i / 3.0 == j) {
                    wynik = wynik + i;

                }
            }

        }
        return wynik;
    }
}

