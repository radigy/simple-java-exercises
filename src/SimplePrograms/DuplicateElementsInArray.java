package SimplePrograms;

import java.util.HashSet;

public class DuplicateElementsInArray {
    public static void main(String[] args) {

        String arr[] = {"java", "C", "C++", "Python", "Java", "Python", "C"};

        // approach 1

//        boolean flag = false;
//        for(int i = 0 ; i < arr.length ; i++) {
//
//            for (int j = i+1 ; j < arr.length ; j++) {
//                if(arr[i] == arr[j]) {
//                    System.out.println("duplicate is: " + arr[i]);
//                    flag = true;
//                }
//            }
//        }
//
//        if (flag=false){
//            System.out.println("duplicate element not found");
//        }
        // approach 2 - hashSet
        HashSet <String> langs = new HashSet();

        boolean flag = false;
        for (String l:arr){
            if(langs.add(l) == false) {
                System.out.println("found duplicate element: " + l);
            }
        }

        if (flag = false){
            System.out.println("there is no duplicates");
        }

    }
}
