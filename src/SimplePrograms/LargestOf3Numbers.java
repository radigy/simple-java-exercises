package SimplePrograms;

import java.util.Scanner;

public class LargestOf3Numbers {
    public static void main(String[] args) {

        Scanner sc= new Scanner(System.in);

        System.out.println("Enter first number: ");
        int a= sc.nextInt();

        System.out.println("Enter second number: ");
        int b = sc.nextInt();

        System.out.println("Enter third number: ");
        int c = sc.nextInt();

        //Approach1 - Logic

//        if(a>b && a>c) {
//            System.out.println(a+  "-a is a largest number");
//        } else if (b>a && b>c) {
//            System.out.println(b+ "-b is a largest number");
//        } else {
//            System.out.println(c+ "-c is a largest number");
//        }

        //Approach2 - Ternary operator
        int largest1 = a>b?a:b;

        int largest2 = c>largest1?c:largest1;

        System.out.println(largest2 + " is the largest");
    }
}
