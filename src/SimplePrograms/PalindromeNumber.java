package SimplePrograms;

import java.util.Scanner;

public class PalindromeNumber {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a number: ");

        int num = sc.nextInt();

        int org_num = num;

        int rev = 0;

        while(num!=0)
        {
            rev=rev*10 + num%10;
            num= num / 10;
        }

//
//        StringBuilder rev;
//
//        StringBuilder sbi = new StringBuilder();
//        sbi.append(num);
//        rev = sbi.reverse();
//        int i = Integer.parseInt(String.valueOf(rev));
//        System.out.println("Reverse number is:" + rev);


        if(org_num==rev) {
            System.out.println(rev + " is a palindrome number");
        } else {
            System.out.println(rev +" is not a palindrome number");
        }


    }
}
