package SimplePrograms;

public class MissingNumberInArray {
    public static void main(String[] args) {

        // array should not have duplicates
        // array no need to be sorted order
        // values should be in range

        int a[] = {1,2,3,4,5,6,7,9,10};

        int sumA =0;

        for (int i = 0;i < a.length ; i++) {
            sumA = sumA + a[i];
        }

        System.out.println("sum of array: " + sumA);

        int sumB =0;

        for (int i= 1 ; i <= 10; i++) {
            sumB = sumB + i;
        }
        System.out.println("sum of range of elements in array: " +sumB);

        System.out.println("the missing number is: " + (sumB-sumA));

    }
}
