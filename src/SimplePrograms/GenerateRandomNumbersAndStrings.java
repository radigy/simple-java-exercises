package SimplePrograms;

import java.util.Random;

public class GenerateRandomNumbersAndStrings {

    public static void main(String[] args) {
        // Approach 1 - Random
//
        Random rand = new Random();

//        int rand_int = rand.nextInt(1000);
//        System.out.println(rand_int);

        double rand_double = rand.nextDouble();
        System.out.println(rand_double);

        // Approach 2 - Math
//        System.out.println(Math.random());


    }
}
