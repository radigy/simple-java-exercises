package SimplePrograms;

import java.util.Scanner;

public class ReverseANumber {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a Number:");

        int num=sc.nextInt();

        System.out.println(num);

        // algorithm
//
//        int rev = 0;
//
//        while(num!=0)
//        {
//            rev=rev*10 + num%10;
//            num= num / 10;
//        }

        // StringBuffer class
//
//        StringBuffer rev;
//
//        StringBuffer sb= new StringBuffer(String.valueOf(num));
//        rev = sb.reverse();
//
//        System.out.println("Reverse number is:" + rev);

        // StringBuilder
        StringBuilder rev;

        StringBuilder sbi = new StringBuilder();
        sbi.append(num);
        rev = sbi.reverse();

        System.out.println("Reverse number is:" + rev);

    }

}
