package SimplePrograms;

public class ReverseEachWordInAString {

    public static void main(String[] args) {

//        approach 1
//        String str = "Welcome To Java";
//
//        String[] words = str.split(" ");
//
//        String reverseString = "";
//
//        for (String w : words) {
//            String reverseWord = "";
//
//            for(int i=w.length()-1; i>=0 ; i--) {
//                reverseWord = reverseWord + w.charAt(i);
//            }
//
//            reverseString=reverseString+reverseWord+" ";
//        }
//       approach 2
        String str = "Welcome To Java";

        String[] words = str.split("\\s");

        String reverseWord= " ";

        for(String w:words){
            StringBuilder sb = new StringBuilder(w);
            sb.reverse();

            reverseWord=reverseWord+sb.toString()+" ";
        }

        System.out.println(reverseWord);

    }

}
