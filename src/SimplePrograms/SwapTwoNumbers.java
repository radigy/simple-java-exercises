package SimplePrograms;

public class SwapTwoNumbers {
    public static void main(String[] args) {
        int a=10;
        int b=20;

        System.out.println("before swapping are.." + a + " " + b);

        //logic1 - third variable

//        int t=a;  // a - 10 b - 20 t - 10
//        a=b;  // a - 20 b - 20 t - 10
//        b=t;  // a- 20 b - 20
//
        // logic 2 - use + & - without using third variable

//        a=a+b;  // a- 30
//        b=a-b; // b - 10
//        a=a-b;  // a-2 0

        // a 20 b 10
//
//        a=a*b; //a 200
//        b=a/b;    //200/20=10
//        a=a/b; //200/10 - 20

        b=a+b-(a=b); // a 20 b 10
        System.out.println("after swapping values are.." + a + " " + b);


    }
}
