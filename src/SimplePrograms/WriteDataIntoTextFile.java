package SimplePrograms;

import java.io.*;

public class WriteDataIntoTextFile {
    public static void main(String[] args) throws IOException {

        FileWriter fw = new FileWriter("C:\\Workspace\\test12");
        BufferedWriter bw = new BufferedWriter(fw);

        bw.write("Selenium with Java");
        bw.write("Selenium with Python");
        bw.write("Selenium with C");

        System.out.println("Finished");

        bw.close();
    }
}
