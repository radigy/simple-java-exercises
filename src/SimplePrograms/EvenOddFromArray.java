package SimplePrograms;

public class EvenOddFromArray {
    public static void main(String[] args) {

        int a[] = {123, 3,44,123,-55};

//        for (int i = 0 ; i < a.length ; i++) {
//
//            if(a[i]%2==0) {
//                System.out.println(i + " is even number");
//            } else {
//                System.out.println(i+ " is odd number");
//            }

            for (int value : a) {
                if(value%2==0) {
                    System.out.println(value + " is even number");
                } else {
                    System.out.println(value+ " is odd number");
                }
        }
    }
}
