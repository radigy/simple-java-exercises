package SimplePrograms;

import java.io.*;
import java.util.Scanner;

public class ReadDataFromTextFile {
    public static void main(String[] args) throws IOException {

        // approach 1

//        FileReader fr = new FileReader("C:\\Workspace\\text.txt");
//        BufferedReader br = new BufferedReader(fr);
//
//        String str;
//
//        while((str = br.readLine()) != null){
//            System.out.println(str);
//        }
//
//        br.close();

        // approach 2

//        File file = new File("C:\\Workspace\\text.txt");
//
//        Scanner sc = new Scanner(file);
//
//        while(sc.hasNextLine()){
//            System.out.println(sc.nextLine());
//        }

        // approach 3
        File file = new File("C:\\Workspace\\text.txt");

        Scanner sc = new Scanner(file);

        sc.useDelimiter("\\Z");
        System.out.println(sc.next());
    }
}
