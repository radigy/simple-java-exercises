package SimplePrograms;

import java.util.Arrays;
import java.util.Collections;

public class SortingElements {

    public static void main(String[] args) {

        // approach 1

//        int a[] = {30,50,20,10,60};

//        System.out.println("array elements before sorting " + Arrays.toString(a));
//        Arrays.parallelSort(a);
//        System.out.println("array after sorting " + Arrays.toString(a));

        // approach 2
//        System.out.println("array elements before sorting " + Arrays.toString(a));
//        Arrays.sort(a);
//        System.out.println("array elements after sorting " + Arrays.toString(a));

        // approach 3 - reverse descending order
        Integer a[] = {30,50,20,10,60};

        System.out.println("array elements before sorting " + Arrays.toString(a));
        Arrays.sort(a, Collections.reverseOrder());
        System.out.println("array elements after sorting " + Arrays.toString(a));
    }

}
