package SimplePrograms;

public class ReverseString {

    public static void main(String[] args) {
        // using + string concatenation opeartor
//
//        String justString = "ABCD";
//
//        String rev = "";
//
//        String[] arrayString ;
//
//        for (int i=justString.length()-1; i >= 0 ; i--) {
//
//            rev=rev + justString.charAt(i);
//            System.out.println(rev);
//        }
//
        // using character array
//
//        String justString = "ABCD";
//
//        char a[]= justString.toCharArray();
//
//        String rev = "";
//
//        for(int i = a.length-1; i>=0 ; i--){
//            rev=rev+a[i];
//        }
//
//        System.out.println("String: " + justString);
//
//        System.out.println("Reversed String: " + rev);
//    }

        // using string buffer class
        String justString = "ABCD";

        StringBuffer sb = new StringBuffer(justString);

        System.out.println(sb.reverse());

    }
}
