package SimplePrograms;

import java.util.Arrays;

public class BubleSort {
    public static void main(String[] args) {

        int a[] = {4,2,122,5,3};

        System.out.println("array before sorting: " + Arrays.toString(a));

        int n = a.length;

        for(int i = 0; i < n-1 ; i++) {  //number of passes
            for(int j = 0; j<n-1; j++) {  //iterations in each pass
                if(a[j]>a[j+1]){
                    int temp = a[j];
                    a[j]=a[j+1];
                    a[j+1] = temp;
                }
            }
        }

        System.out.println("array after sorting: " + Arrays.toString(a));

    }
}
