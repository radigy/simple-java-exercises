package SimplePrograms;

public class RemoveWhiteSpaces {

    public static void main(String[] args) {

        String str = "java       programming selenium      automation";

        System.out.println("Before removing whistespaces: " + str);

        str = str.replaceAll("\\s", "");

        System.out.println("After removing whistespaces: " + str);
    }

    
}
