package SimplePrograms;

public class SumOfArray {

    public static void main(String[] args) {
        int a[] = {1,10,100,1000};

        int sum=0;

        // for loop
//        for(int i=0; i <= a.length-1 ; i++) {
//            sum=sum+a[i];
//        }

        for(int value:a){
            sum=sum+value;
        }

        System.out.println("Sum of array elements: " + sum);
    }

}
