package SimplePrograms;

public class RemoveJunk {
    public static void main(String[] args) {

        String s = "!!#$%&()*+,-./:;<=>?@ latin string 01234567890";

        String s1 = "!@$!@$!@$!@%!@ seleniUm 12$$@!4 java";

        s = s.replaceAll("[^a-zA-Z0-9]", "");

        System.out.println(s);

        s1 = s1.replaceAll("[^a-z0-9]", "");

        System.out.println(s1);

    }
}
