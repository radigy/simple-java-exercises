package SimplePrograms;

import java.util.Scanner;

public class PalindromeString {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the string: ");

        String str = sc.next();

        String org_str = str;

        String rev = "";

        String[] arrayString ;

        for (int i=str.length()-1; i >= 0 ; i--) {

            rev=rev + str.charAt(i);
            System.out.println(rev);
        }

        if(org_str.equals(rev)) {
            System.out.println(str + "String is palindrome");
        } else {
            System.out.println(str + "String is not palindrome");
        }
    }

}
